var app = angular.module('frontend',[]);
app.filter('prettyJSON', function () {
    function prettyPrintJson(json) {

        return JSON ? JSON.stringify(json, null, '  ') : 'your browser doesnt support JSON so cant pretty print';
    }
    return prettyPrintJson;
});
app.controller('myController', function($scope, $http) {
    $scope.data = [];
    $scope.json = [];
    var vm = this;
    vm.filters = [];
    vm.addValue = function (value) {
        if(!filter.value instanceof Array) {
            filter.value = [];
        };
        filter.value.push(value);
        $scope.value = "";
    };
    vm.filterRequest =  function (sort, page, limit, reverse) {
        let data = {
            sort: sort || null,
            page: page || 0,
            limit: limit || 20,
            reverse: (reverse == 'true'?true:false) || false,
            filters: vm.filters
        };
        var request = $http.post('/api/filter/', data);
        request.success(function(data) {
            $scope.json = data;
        });
        request.error(function(data){
            $scope.json = data;
            console.log('Error:' + data);
        });
    };
    vm.startStreaming =  function (key) {
        var request = $http.post('/api/start', {"key": key});
        request.success(function(data) {
            $scope.json = data;
        });
        request.error(function(data){
            $scope.json = data;
            console.log('Error: ' + data);
        });
    };
    vm.addFilter = function (filter) {
        vm.filters.push(filter);
        $scope.filter = {};

    };
});