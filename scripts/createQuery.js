var _ = require('underscore');

module.exports = function (filter, done) {
    if(!filter.hasOwnProperty('operator') || !filter.hasOwnProperty('value')) {
        done('operator, value, field is neccessary parameters for filters', null);
    }
    if(!filter.hasOwnProperty('field') && filter.operator !== 'textsearch') {
        done('field is required', null);
    }
    if (filter.operator == 'gt') {
        if (_.contains(['favoritecount', 'replycount', 'retweetcount', 'timestamp_ms', 'created_at'], filter.field)) {
            var query = { '$gt': filter.value } ;
            return done(null, [filter['field'],query]);
        } else {
            return ('field not valid for greater then operation', null);
        }
    }
    if (filter.operator == 'lt') {
        if (_.contains(['favoritecount', 'replycount', 'retweetcount', 'timestamp_ms', 'created_at'], filter.field)) {
            var query = {'$gt': filter['value']};
            return done(null, [filter['field'],query]);
        } else {
            return ('field not valid for less then operation', null);
        }
    }
    if(filter.operator === 'textsearch') {
        var query = {"$search": filter["value"]};
        return done(null, ["$text", query]);
    }
    if(filter.operator === 'contains') {
        var query = {"$regex": filter['value']};
        return done(null, [filter['field'],query]);
    }
    if(filter.operator === 'startwith') {
        var query = {"$regex": '^'+filter['value']};
        return done(null, [filter['field'],query]);
    }
    if(filter.operator === 'endwith') {
        var query = {"$regex": filter['value']+'$'};
        return done(null, [filter['field'],query]);
    }
    if(filter.operator === 'eq') {
        return done(null, [filter['field'],filter['value']]);
    }
    if (filter.operator === 'in') {
            var query =  {'$in': filter['value']};
            return done(null, [filter['field'],query]);
    }
};

