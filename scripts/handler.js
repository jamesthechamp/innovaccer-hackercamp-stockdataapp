
var Tweet = require('../models/Tweet');
module.exports = {
    insertMultipleDoc: function (tweets, done) {
        let tweetsObj = tweets.map(function(data, x) {
            let hashtags = [];
            if (data['entities']['hashtags'] instanceof Array){
                hashtags = data['entities']['hashtags'].map( value => value['text']);
            }
            let country = (data.hasOwnProperty('place') && data.place != null ? data['place']['country']: null);
            let country_code = (data.hasOwnProperty('place') && data.place != null ? data['place']['country_code']: null);
            // Construct a new tweet object
            return {
                twid: data['id'],
                author: data['user']['name'],
                avatar: data['user']['profile_image_url'],
                body: data['text'],
                date: data['created_at'],
                screenname: data['user']['screen_name'],
                username: data['user']['name'],
                location     : data['user']['location'],
                sourcedevice : data['source'],
                isretweeted  : data['retweeted'],
                retweetcount : data['retweet_count'],
                country      : country,
                countrycode  : country_code,
                replycount   : data['replay_count'],
                favoritecount: data['favorite_count'],
                created_at: data['created_at'],
                lang: data['lang'],
                hashtags: hashtags
            };
        });
        console.log('temp');
        try {
            tweetDoc = Tweet.create(tweetsObj).then(()=>{
                done(null, tweetDoc);
            }).catch((err)=>{
                console.log('err',err);
                done(err. null);
            });
        } catch(err) {
            console.log(err, null);
            done(err);
        };
    },
    streamHandler: function(stream, io){

        // When tweets get sent our way ...
        stream.on('data', function(data) {
            hashtags = [];
            // if data['entities']['hashtags']:
            // hashtags.append(hashtag['text'])
            let country = (data.hasOwnProperty('place') && data.place != null ? data['place']['country']: null);
            let country_code = (data.hasOwnProperty('place') && data.place != null ? data['place']['country_code']: null);
            // Construct a new tweet object
            let tweet = {
                twid: data['id'],
                author: data['user']['name'],
                avatar: data['user']['profile_image_url'],
                body: data['text'],
                date: data['created_at'],
                screenname: data['user']['screen_name'],
                username: data['user']['name'],
                location     : data['user']['location'],
                sourcedevice : data['source'],
                isretweeted  : data['retweeted'],
                retweetcount : data['retweet_count'],
                country      : country,
                countrycode  : country_code,
                replycount   : data['replay_count'],
                favoritecount: data['favorite_count'],
                created_at: data['created_at'],
                timestamp_ms: new Date(parseInt(data['timestamp_ms'])),
                lang: data['lang'],
                hashtags: hashtags

            };

            // Create a new model instance with our object
            var tweetEntry = new Tweet(tweet);

            // Save 'er to the database
            tweetEntry.save(function(err) {
                if (!err) {
                    console.log("saved twet", tweet.twid);
                    // If everything is cool, socket.io emits the tweet.
                    // io.emit('tweet', tweet);
                } else {
                    console.log("error with twid:", tweet.twid, "error", err.message);
                }
            });

        });

    }
};