
var Twitter = require('twitter');
var credentials = require('../credentials.json');
var handler = require('./handler');
var app = require('../app.js');

var twit = new Twitter({
    consumer_key: credentials.consumer_key,
    consumer_secret: credentials.consumer_secret,
    access_token_key: credentials.access_token_key,
    access_token_secret: credentials.access_token_secret
});


module.exports = {
    searchQuery: function (keyword,done) {
        let params = {
            q: keyword,
            result_type: 'popular',
            count: 100
        };
        twit.get('search/tweets.json', params, function (error, tweets, response) {
            if(error) {
                done(error, null);
            } else {
                handler.insertMultipleDoc(tweets.statuses, function (err, data) {
                    if(err) {
                        done(err, null);
                    } else {
                        done(null, data);
                    }
                });
            }
        });
    },
    startStream: function (keyword) {
        try {
            stream = twit.stream('statuses/filter', {track: keyword + ', #' + keyword});
            handler.streamHandler(stream, app.io);
            setTimeout(function () {
                console.log("stream destroyed for", keyword);
                stream.destroy();
            }, 300000);
        } catch (e) {
            console.log("error", e);
        }
    }
}

