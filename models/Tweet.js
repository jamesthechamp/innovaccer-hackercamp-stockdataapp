var mongoose = require('mongoose');

// Create a new schema for our tweet data
var schema = new mongoose.Schema({
    twid         : String,
    author       : {type: String, index: true},
    avatar       : String,
    body         : {type: String, index: true},
    date         : Date,
    screenname   : {type: String, index: true},
    username     : {type: String, index: true},
    location     : String,
    sourcedevice : String,
    isretweeted  : Boolean,
    retweetcount : Number,
    country      : String,
    countrycode  : String,
    replycount   : Number,
    favoritecount: Number,
    created_at: Date,
    timestamp_ms: Date,
    lang: String,
    hashtags: [{ type: String, index: true}]
});

schema.index({ author: 'text', body: 'text', screenname: 'text', username: 'text'});
// Create a static getTweets method to return tweet data from the db


// Return a Tweet model based upon the defined schema
module.exports = Tweet = mongoose.model('Tweet', schema);
