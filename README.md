# Innovaccer-hackercamp-stockdataapp

author: Shivansh Bajaj

The is assignment solution for question statement provided for innovaccer hackercamp

    stack : NodeJS(ExpressJS), MongoDB, AngularJS

####Discription:

**Stream/search API:**  
Data is extracted from both tweeter **stream** as well as **search API's**  
(as tweeter stream provide data created after API is called so most filters of numeric field(like retweetcount, favioritecount, etc) would be of no use as when tweet is created this field will be 0. So here search API is used to provided data with such fields)

**Filter API:**
here filters work on an operator based machanism which require this three parameters use for filtering.  
```
    field: fieldname
    operator: type of filter operation,  
    value: value to compare data with  
```
####Fields:  
fieldName: Type

```
    author       : String,(author name)
    avatar       : String,(user profile picture URL)
    body         : String,(tweet text)
    date         : Date,(date tweet created)
    screenname   : String,(user twitter screenname)
    username     : String,(user twitter username)
    location     : String,(user location)
    sourcedevice : String,(tweet source device)
    isretweeted  : Boolean,(is tweet retweeted)
    retweetcount : Number,(tweet count)
    country      : String,(user country name)
    countrycode  : String,(user country code)
    replycount   : Number,(tweet replycount)
    favoritecount: Number,(tweet favorite count)
    lang         : String,(language of tweet) 
    hashtags     : [String](hashtags associated to tweet)
```

operations:  

eq: return elements with field equal to value  

```    
    value type: field type,
    valid field: All
```  

in: return elements with field value in array of provided values  
 
```
    value type: Array of field type,
    valid field: all except for hashtags
``` 
   
gt: return elements with field value greater then input value  

```
       value type: number,
       valid field: 'favoritecount', 'replycount', 'retweetcount', 'date'
```
lt: return elements with field value less then input value  

```
       value type: number,
       valid field: 'favoritecount', 'replycount', 'retweetcount', 'date'
```

contains: search for provided field contains provided value(string) .  

```
        value type: string,
        valid field: all string field(body, lang, author, username, country, countrycode)
```

startwith: search for provided field startwith provided value(string) .  

```
        value type: string,
        valid field: all string field(body, lang, author, username, country, countrycode)
```

endwith: search for provided field endwith provided value(string) .  

```
        value type: string,
        valid field: all string field(body, lang, author, username, country, countrycode)
```

textsearch: search for provided value(string) in all fields.  

```
        value type: string,
        valid field: None(no field input needed)
```


####requirement
    nodeJS == 9.3.0 & npm == 5.6.0

####run:
cd to repo

    npm install
    npm start

####API 1:  
start streaming and searching from twitter for given keyword.  
request format:   

```
    header: {
        content-type: "application/json"
    }
    accesspoint: /api/start   
    method: ['GET']
    params = {   
        "q": <string>  
    } 
```

response format:  

```
    type: json
    { 
        "status": "success" / "failed",  
        "msg": <string>,  
        "data": <string> (top 100 popular tweets with keyword)  
    }
```

####API2:  
to filter data according to parameter

request format:  

```
    header: {
        content-type: "application/json"
    }
    accesspoint: /api/filterfield/<fieldName>  
    method: ['GET']
    params = {
        "value": <string>/<number>/[array],
        "operator": <string> (can be eq, gt, lt),
        "format": "csv"/"json"
    }  
```

response format: 

```
    type: json
    {
        "status": "success"/"fail",
        "msg": <string>,
        "data": <data format>   
    }
```

example request format:   

```
    header: {
            content-type: "application/json"
        }
        accesspoint: /api/filterfield/screenname
        method: ['GET']
        params = {
            "value": modi,
            "operator": eq,
            "format": "json"
        } 
```

####API3:  
to filter multiple field together
for operation on multiple field you can use this API by sending a post query with filter details

filter object:   

```
    {
        "operator": <string>, (anything of operators mention above)
        "field": <string>,(this could be anything from fieldNames define in schema)
        "value": <string>/<number>/[array],
    } 
```
request format:  

```
    header: {
        content-type: "application/json"
    }
    accesspoint: /api/filter,
    method: ['POST'],
    body: {
        filter:[filter Objects],
        sort:<string>,(sort key)
        limit: <number>,
        page: <number>,
        reverse: <boolean>,
        format: "csv"/"json"
    }
```

response format: 

```
    type: json
    {
        "status": "success"/"fail",
        "msg": <string>,
        "data": <data format>   
    }
```

example Request:  

```
    {
         header: {
                 content-type: "application/json"
             }
             accesspoint: /api/filter,
             method: ['POST'],
             body: {
                 "sort": "date",
                 "limit": 20,
                 "page": 1,
                 "reverse": false,
                 "format": "json",
                 "filter":[
                    {
                        "operator": "eq",
                        "field": "screenname",
                        "value": "modi"
                    },
                    {
                        "operator: "gt",
                        "field": "retweetcount",
                        "value": 10
                    }
                 ]
    }
```

####Note: Text search:  

text search feature can be used in API 3. with filter text search API will search for value in every field and return documents with text.
As it search in all field no field is require
filter object in case of text search:  

```
    {
        "operator": "textsearch",
        "value": <string>
    }
```

Example request for text search:  

```
{
      header: {
              content-type: "application/json"
          }
      accesspoint: /api/filter,
      method: ['POST'],
      body: {
          "sort": "date",
          "limit": 20,
          "page": 1,
          "reverse": false,
          "format": "json",
          "filter":[
             {
                 "operator": "textsearch",
                 "value": "modi"
             },
             {
                 "operator: "gt",
                 "field": "retweetcount",
                 "value": 10
             }
          ]
     }
}
```

####Note: csv output:
we can get response in json format or csv file from API 2 and API 3.  


**To get response with csv file with API2:**  

**request:**  

```
    header: {
        content-type: "application/json"
    }
    accesspoint: /api/filterfield/<fieldName>  
    method: ['GET']
    params = {
        "value": <string>/<number>/[array],
        "operator": <string> (can be eq, gt, lt),
        "format": "csv"
    } 
```

**To get response with csv in API 3:**  

**request:**  

```
{
  header: {
          content-type: "application/json"
      }
  accesspoint: /api/filter,
  method: ['POST'],
  body: {
      "sort": "date",
      "limit": 20,
      "page": 1,
      "reverse": false,
      "format": "csv",
      "filter":[
         {
             "operator": "textsearch",
             "value": "modi"
         },
         {
             "operator: "gt",
             "field": "retweetcount",
             "value": 10
         }
      ]
  }
}
```