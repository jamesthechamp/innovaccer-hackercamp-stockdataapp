const async = require('async');
const express = require('express');
const tweet = require('../models/Tweet');
const router = express.Router();
var jsonexport = require('json-2-csv');
const createStream = require('../scripts/twitterApi');
const createQuery = require('../scripts/createQuery');
var _ = require('underscore');

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', { title: 'tweet streaming application' });
});

router.post('/api/start', function (req, res) {
    if(req.body.hasOwnProperty('key') && req.body.key != null) {
        async.auto({
                'search': function (callback) {
                    createStream.searchQuery(req.body.key, function(err, tweets){
                            if(err) {
                                callback(err, null);
                            } else{
                                callback(null, tweets);
                            }
                        }
                    );
                },
                'stream': ['search',function (tweets, callback) {
                    createStream.startStream(req.body.key);
                    callback(null, tweets);
                }]
            },
            function(err, tweet) {
                if(err) {
                    return res.status(200).json({
                        'success': 'success',
                        'msg': 'error while searching for old post. But new tweets still working'
                    })
                } else {

                    return res.status(200).json({
                        'status': 'success',
                        'msg': 'stream start',
                    })
                }
            });
    } else {
        return res.status(200).json({
            "status": "fail",
            "msg": "key parameter is required keyword"
        })
    }
});

router.get('/api/filterfield/:field', function (req, res) {
    if(req.params.hasOwnProperty('field') && _.contains(['author',
            'avatar',
            'body',
            'screenname',
            'username',
            'location',
            'sourcedevice',
            'isretweeted',
            'retweetcount',
            'country',
            'countrycode',
            'replycount',
            'favoritecount',
            'created_at',
            'timestamp_ms', 'lang', 'hashtags'], req.params.field)){
        if(req.query.hasOwnProperty('value')) {
            async.waterfall([function (next) {
                let filter = {
                    'operator': req.query.operator || 'eq',
                    'field': req.params.field,
                    'value': req.query.value
                };
                var query = {};
                createQuery(filter, (err, result) => {
                    if (err) {
                        next(err, null);
                    } else {
                        query[result[0]] = result[1];
                        next(null, query);
                    }
                });
            }, function (query, done) {
                let sort = req.body['sort'] || 'created_at',
                    page = parseInt(req.body['page']) || 0,
                    limit = parseInt(req.body['limit']) || 20,
                    reverse = req.body.reverse || false,
                    sortString = {};
                if(reverse) {
                    sortString[sort] = -1;
                } else {
                    sortString[sort] = 1;
                }
                tweet.find(query).limit(limit).skip(page*limit).sort(sortString).exec((err, twit) => {
                    if(err) {
                        done(err.message, null);
                    } else {
                        done(null, twit);
                    }
                });
            }, function (twits, done) {
                if (req.query.hasOwnProperty('format') && req.query.format === 'csv') {

                    let tweetsObj = twits.map(function(data, x) {
                        return {
                            twid: data['twid'],
                            author: data['author'],
                            avatar: data['user'],
                            body: data['body'],
                            date: data['date'],
                            screenname: data['screenname'],
                            username: data['username'],
                            location     : data['location'],
                            sourcedevice : data['sourcedevice'],
                            isretweeted  : data['isretweeted'],
                            retweetcount : data['retweetcount'],
                            country      : data['country'],
                            countrycode  : data['country_code'],
                            replycount   : data['replaycount'],
                            favoritecount: data['favoritecount'],
                            created_at: data['created_at'],
                            lang: data['lang'],
                            hashtags: data['hashtags']
                        };
                    });
                    // const exports = req.query.exports || Object.keys(tweet.schema.paths);
                    jsonexport.json2csv(tweetsObj, function (err, csv) {
                        console.log(csv);
                        if(err) {
                            done(err, null, 'csv');
                        } else {
                            done(null, csv, 'csv');
                        }
                    });
            } else {
                done(null, twits, 'json')
            }
        }], function (err, twit, format){
            if(err) {
                return res.json({
                    'status': 'fail',
                    'msg': err
                });
            } else {
                if(format === 'csv') {
                    res.attachment('data.csv');
                    res.status(200).send(twit);
                } else {
                    return res.status(200).json({
                        'status': 'success',
                        data: twit
                    });
                }
            }
        });
} else {
    return res.json({
        'status': 'failed',
        'msg': 'value field is required in params'
    })
}
} else {
    return res.json({
        'status': 'failed',
        'msg': 'field not found'
    })
}

});

router.post('/api/filter', function (req, res) {
    async.waterfall([function (next) {
        var query = {};
        if(req.body.hasOwnProperty('filters') && req.body.filters) {
            async.each(req.body.filters, (filter, callback) => {
                createQuery(filter, (err, result) => {
                    if (err) {
                        callback(err);
                    } else {
                        query[result[0]] = result[1];
                        callback()
                    }
                });
            }, (err) => {
                if(err) {
                    next(err, null);
                } else {
                    next(null, query);
                }
            });
        } else {
            next(null, query);
        }
    }, function (query, done) {
        let sort = req.body['sort'] || 'created_at',
            page = parseInt(req.body['page']) || 0,
            limit = parseInt(req.body['limit']) || 20,
            reverse = req.body.reverse || false,
            sortString = {};
        if(reverse) {
            sortString[sort] = -1;
        } else {
            sortString[sort] = 1;
        }
        tweet.find(query).limit(limit).skip(page*limit).sort(sortString).exec((err, twit) => {
            if(err) {
                done(err.message, null);
            } else {
                done(null, twit);
            }
        });
    }, function (twits, done) {
        if (req.body.hasOwnProperty('format') && req.body.format === 'csv') {
            let tweetsObj = twits.map(function(data, x) {
                return {
                    twid: data['twid'],
                    author: data['author'],
                    avatar: data['user'],
                    body: data['body'],
                    date: data['date'],
                    screenname: data['screenname'],
                    username: data['username'],
                    location     : data['location'],
                    sourcedevice : data['sourcedevice'],
                    isretweeted  : data['isretweeted'],
                    retweetcount : data['retweetcount'],
                    country      : data['country'],
                    countrycode  : data['country_code'],
                    replycount   : data['replaycount'],
                    favoritecount: data['favoritecount'],
                    created_at: data['created_at'],
                    lang: data['lang'],
                    hashtags: data['hashtags']
                };
            });
            // const exports = req.query.exports || Object.keys(tweet.schema.paths);
            jsonexport.json2csv(tweetsObj, function (err, csv) {
                console.log(csv);
                if(err) {
                    done(err, null, 'csv');
                } else {
                    done(null, csv, 'csv');
                }
            });
        } else {
            done(null, twits, 'json')
        }
    }], function (err, twit, format){
        if(err) {
            return res.json({
                'status': 'fail',
                'msg': err
            });
        } else {
            if(format === 'csv') {
                res.attachment('data.csv');
                res.status(200).send(twit);
            } else {
                return res.status(200).json({
                    'status': 'success',
                    data: twit
                });
            }
        }
    });
});

module.exports = router;
